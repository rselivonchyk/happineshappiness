import React, { Component } from 'react';
//import './App.css';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Navbar from './Components/Navbar';
import Main from './Components/Main';
import './styles.scss';
import "animate.css";
import Results from './Components/Results'

// const countries = require('country-list')();

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
         <Navbar />
            <Switch>
              <Route path='/results' component={Results} />
              <Route path='/' component={Main} />
            </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
