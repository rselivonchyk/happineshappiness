import React, { Component } from 'react'
import Qualities from "./Qualities";

export default class QualityList extends Component {
  constructor(props){
    super();
    this.state = {
      clicks: 0
    }
  }

  clickCounter = (clicked) => {
 
    if(clicked) {
      this.setState({
        clicks : this.state.clicks + 1
      })
    } else {
      this.setState({
        clicks : this.state.clicks - 1
      })
    }
    
  }
 
  
  render() {
    console.log(this.state.clicks)
    const qualities = [1,2,3,4,5,6,7,8,9];
    return (
      <>
      <section className="list">
        {qualities.map((item,i) =>
          <Qualities quality={item} key={i} id={i} clickCounter={this.clickCounter} />
          )}
         
      </section>
       <button className={this.state.clicks>2 ? "button" : "hidden"}> DONE </button>
       </>
    )
  }
}
