import React, { Component } from 'react'

export default class Qualities extends Component {
    constructor(props){
        super();
        this.quality  = React.createRef();
        this.state = {
            isClicked : false
        }
    }
    clicked = () => {
        
        this.setState({
            isClicked : !this.state.isClicked
        })
        this.props.clickCounter(!this.state.isClicked);

        
    }
  render() {
      const {isClicked} = this.state

    return (
      <div className={isClicked ? "clicked" : "list__quality" } ref={this.quality} onClick={this.clicked} id={this.props.id}>
        <h1>{this.props.quality}</h1>
      </div>
    )
  }
}
