import React, { Component } from 'react';
import Map from './Map';

class Navbar extends Component {
  render() {
    return (
      <div className="navbar animated fadeInDown delay-0.5s">
        <h2>HEADER</h2>
        <ul>
          <li>Home</li>
          <li>About</li>
          <li></li>
        </ul>
      </div>
    );
  }
}

export default Navbar;