import React, { Component } from 'react';
import './Results.scss';
import Norway from '../Pictures/norway.jpg'
var ReactDOM = require('react-dom');

class Results extends Component {
  
  render() {
    const paragraph = `Fun Fact: Sweden is so good at recycling, it now runs out of garbage to recycle.`      
    const data = [
      {
        title: 'Norway',
        coordinates: [53.902496, 27.561481],
        paragraph,
        img:"https://i.imgur.com/HJh0zzD.jpg"
      },
      {
        title: 'Singapore',
        coordinates: [38.899513, -77.036527],
        paragraph,
        img:"https://i.imgur.com/HJh0zzD.jpg"
      },
      {
        title: 'Luxembourg ',
        coordinates: [55.755814, 37.617635],
        paragraph,
        img:"https://i.imgur.com/HJh0zzD.jpg"
      }
    ]

    class MapComponent extends React.Component {
      render () {
        const { 
          props: {
            chosenItemKey,
            handleOnInited
          }
        } = this
        
        const {
          coordinates
        } = data[chosenItemKey]
        console.log(data[1].img);
        return (
          <div {...{ className: 'map-wrapper'}}>
            <img src={Norway} alt="country"></img>
          </div>
        )
      }
    }

    class ItemsList extends React.Component {
      constructor (props) {
        super(props)
        this.contentRef = React.createRef()
        this.mapBgRef = React.createRef()
        this.titleBgRef = React.createRef()
      }
      
      handleItemClick = (key) => {
        console.log(this.titleBgRef.current)
        this.titleBgRef.current.setAttribute(
          'style', 
          'transform-origin: right; transform: scaleX(0);'
        )
        this.mapBgRef.current.setAttribute(
          'style', 
          'transform: scaleX(25);'
        )
        this.contentRef.current.setAttribute(
          'style', 
          'opacity: 0; max-height: 0; transform: translateY(-7rem);'
        )
        
        setTimeout(() => {
          this.props.handleItemChoose(key)
          setTimeout(() => {
            this.contentRef.current.setAttribute(
              'style', 
              'opacity: 1; max-height: 20rem; transform: translateY(0);'
            )
            this.mapBgRef.current.setAttribute(
              'style', 
              'transform: scaleX(1);'
            )
            this.titleBgRef.current.setAttribute(
              'style', 
              'transform-origin: left; transform: scaleX(1);'
            )
          }, 10)
        }, 600)    
      }
      
      componentDidUpdate = (prevProps) => {
        if (!prevProps.inited && this.props.inited) {
          this.titleBgRef.current.setAttribute(
            'style', 
            'transform: scaleX(1);'
          )
          this.contentRef.current.setAttribute(
            'style', 
            'opacity: 1; max-height: 20rem; transform: translateY(0);'
          )
          this.mapBgRef.current.setAttribute(
            'style', 
            'transform: scaleX(1);'
          )
        }
      }
      
      render () {
        const {
          props: {
            handleItemChoose,
            chosenItemKey
          }
        } = this
        
        return (
          <React.Fragment>
            <span {...{ className: 'map-bg', ref: this.mapBgRef }} />
            <ul {...{ className: 'list' }}>
              {data.map(({title, paragraph}, key) => {
                const opened = chosenItemKey === key
                return (
                  <li
                    {...{
                      onClick: () => { this.handleItemClick(key) },
                      key,
                      className: `list-item ${opened && 'opened'}`
                    }}
                  >
                    <h3 {...{ className: 'list-item__title' }}>
                      <span {...{ className: 'list-item__title-text' }}>
                        {title}
                        {opened
                          ? <span {...{ className: 'list-item__title-bg', ref: this.titleBgRef }} />
                          : ''
                        }
                      </span>
                    </h3>
                    {opened
                      ? <div {...{ className: 'list-item__content',  ref: this.contentRef }}>
                        <p
                          {...{
                            className: 'list-item__paragraph'
                          }}
                        >
                          {paragraph}
                        </p>
                      </div>
                      : '' // WTF codepen hack
                    }
                  </li>
                )
              })}
            </ul>
          </React.Fragment>
        )
      }
    } 


    class NewComponent extends React.Component {
      state = {
        chosenItemKey: 1,
        inited: false
      }
      
      handleItemChoose = (newKey) => {
        this.setState({ chosenItemKey: newKey })
      }
      
      handleOnInited = () => {
        this.setState({ inited: true })
      }
      
      render () {
        const {
          state: {
            chosenItemKey,
            inited
          },
          handleItemChoose,
          handleOnInited
        } = this
        
        return (
          <div {...{ className: 'wrapper' }}>
            <ItemsList {...{ chosenItemKey, inited, handleItemChoose }} />
            <MapComponent {...{ chosenItemKey, handleOnInited }} />
          </div>
        )
      }
    }

    return (
      <div className="results">
        <NewComponent />
      </div>
    );
  }
}

export default Results;

