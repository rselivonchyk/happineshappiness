import React, { Component } from 'react';
import QualityList from './QualityList';

class Main extends Component {
  render() {
    return (
      <div className="main">
        <QualityList/>
      </div>
    );
  }
}

export default Main;